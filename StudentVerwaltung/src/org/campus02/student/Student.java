package org.campus02.student;

public class Student
{
	private String name;
	
	public Student(String name)
	{
		this.name=name;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
	
	public String toString()
	{
		return String.format("Student[name:%s ]", this.name);
	}

}
