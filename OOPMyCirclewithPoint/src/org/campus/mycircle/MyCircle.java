package org.campus.mycircle;



public class MyCircle
{

	private Point point;
	private int radius;
	
	public MyCircle()
	{
		this.point=new Point(0,0);
		this.radius=1;
	}
	
	public MyCircle(int x,int y, int radius)
	{
		
		this.point=new Point(x,y);
		this.radius=radius;
	}
	
}
