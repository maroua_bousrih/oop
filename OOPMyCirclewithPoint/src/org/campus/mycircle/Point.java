package org.campus.mycircle;

public class Point
{
	private int x;
	private int y;
	
	public Point()
	{
		this.x=0;
		this.y=0;
	}
	public Point(int x,int y)
	{
		this.x=x;
		this.y=y;
	}
	public int getX()
	{
		return this.x;
	}
	public void setX(int x)
	{
		this.x = x;
	}
	public int getY()
	{
		return this.y;
	}
	public void setY(int y)
	{
		this.y = y;
	}
	
	public void setXY(int x,int y)
	{
		this.x=x;
		this.y=y;
	}
	public int[] getXY()
	{
		int[] array=new int[2];
		array[0]=this.x;
		array[1]=this.y;
		return array;
	}
	
	public String toString()
	{
		return String.format("{x:%d, y:%d}", this.x,this.y);
	}
	
	public double distance(int x,int y)
	{
		double result;
		
		result=Math.sqrt((this.x-x)*(this.x-x) + (this.y-y)*(this.y-y));
		return result;
		
	}
	
	public double distance(Point point)
	{
		double result;
		
		result=Math.sqrt((this.x-point.x)*(this.x-point.x) + (this.y-point.y)*(this.y-point.y));
		return result;
		
	}
	public double distance()
	{
		double result;
		
		result=Math.sqrt((this.x-0)*(this.x-0) + (this.y-0)*(this.y-0));
		return result;
		
	}	

}
