import org.campus02.filesystem.Datei;
import org.campus02.filesystem.Verzeichnis;

public class VerzeichnisApp
{

	public static void main(String[] args)
	{
	
		Verzeichnis java= new Verzeichnis("java");
		System.out.println(java);
		Verzeichnis java_exercice= new Verzeichnis("java_exercice");
		Verzeichnis java_courses= new Verzeichnis("java_courses");
		
		Datei OOP1 =new Datei("PPO1 courses",1900);
		Datei OOP2 =new Datei("PPO2 courses",2900);
		
		
		Datei OOP1_exercice =new Datei("PPO1 exercices",1000);
		Datei OOP2_exercice =new Datei("PPO2 exercices",500);

		

		java.addVerzeichnis(java_courses);
		java_courses.addFile(OOP1);
		java_courses.addFile(OOP2);
		System.out.println(java_courses);
		System.out.println(java_courses.SummeAlleDatei());
		
		
		
		System.out.println("******************");
		java.addVerzeichnis(java_exercice);
		java_exercice.addFile(OOP1_exercice);
		java_exercice.addFile(OOP2_exercice);
		System.out.println(java_exercice);
		System.out.println(java_exercice.SummeAlleDatei());
		
		System.out.println("*************");
		Datei readme=new Datei("Read_me",900);
		java.addFile(readme);
		System.out.println(java.sizeAlleDateiVerzeichnis());
		System.out.println("*******************");
		System.out.println(java);
		System.out.println();
		

		System.out.println("alle size von alle datei und verzeichnis "+ java.SummeAlleDatei());


		
	}

}
