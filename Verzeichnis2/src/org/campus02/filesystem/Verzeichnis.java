package org.campus02.filesystem;

import java.util.ArrayList;

public class Verzeichnis
{
	private String name;
	private ArrayList<Datei> list=new ArrayList<Datei>();
	private ArrayList<Verzeichnis> list_verzeichnis=new ArrayList<Verzeichnis>();
	
	public Verzeichnis(String name)
	{
		this.name=name;
	
	}
	
	public void addFile(Datei file)
	{
		list.add(file);
	}
	
	public void addVerzeichnis(Verzeichnis ver)
	{
		list_verzeichnis.add(ver);
	}
	
	public Datei FindBigFile()
	{
		Datei max = list.get(0);
		
		for(Datei wert:list)
		{
			if(wert.getSize()>max.getSize())
			{
				max=wert;
				
			}
			
		}
		return max;
	}
	//give the summ of all file
	public int sizeAlleDateiVerzeichnis()
	{
		int summe=0;
		if(list_verzeichnis!=null)
		{
			for(Datei wert:list)
			{
				
					summe += wert.getSize();
					
			}
				
		}
		return summe;
	}
	
	public int SummeAlleDatei()
	{
		int summe=0;
		if( list != null)
		{
			summe=sizeAlleDateiVerzeichnis();
		}
		 if (list_verzeichnis!=null)
		{		
			for(Verzeichnis wert:list_verzeichnis)
			{
				summe += wert.sizeAlleDateiVerzeichnis();
			}
			
		}
		if(list_verzeichnis!=null && list != null )
		{
			summe = sizeAlleDateiVerzeichnis();
			for(Verzeichnis wert:list_verzeichnis)
			{
				summe += wert.sizeAlleDateiVerzeichnis();
			}
			
		
		}
		return summe;
		
	}
	
	
	public String toString()
	{
		if(list_verzeichnis.isEmpty()&&list.isEmpty())
		{
			return String.format("Verzeichnis[name: %s ]", this.name);
		}
			
		if(list_verzeichnis.isEmpty()&&!list.isEmpty())
		{
			return String.format("Verzeichnis[name: %s, listDatei: %s]", this.name,this.list);
		}
		
		if(!list_verzeichnis.isEmpty()&&!list.isEmpty())
		{
			return String.format("Verzeichnis[name: %s, listDatei: %s, listUnterVerzeichnis:%s]", this.name,this.list,this.list_verzeichnis);
		}
		else
			return null;
		
	}
	

}
