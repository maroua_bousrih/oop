package org.campus02.filesystem;

public class Datei
{
	private String name;
	private int size;
	
	
	public Datei(String name, int size)
	{
		this.name=name;
		this.size=size;
		
	}

	public int getSize()
	{
		return size;
	}

	public String getName()
	{
		return name;
	}

	public String toString()
	{
		return String.format("Datei[name:%s , size: %d]", name,size);
	}
	
	

}
