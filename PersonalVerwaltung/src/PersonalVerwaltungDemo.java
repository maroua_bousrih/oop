import org.campus02.emp.Employee;
import org.campus02.emp.EmployeeManager;


public class PersonalVerwaltungDemo
{
	public static void main(String[] args)
	{
		EmployeeManager infineon =new EmployeeManager(); 
		
		Employee first = new Employee(0001,"Maroua","IT",2100d);
		Employee second = new Employee(0002,"Bassem","IT",2900d);
		Employee third = new Employee(0003,"doudou","Security",3000d);
		Employee fourth = new Employee(0004,"nounou","Test",1900d);
		infineon.addEmployee(first);
		System.out.println(first);
		infineon.addEmployee(second);
		System.out.println(second);
		infineon.addEmployee(third);
		System.out.println(third);
		infineon.addEmployee(fourth);
		System.out.println(fourth);
		System.out.println();
		
		System.out.println(infineon.findByEmpNumber(0004)); 
		System.out.println();
		System.out.println(infineon.findByDepartement("IT"));
		System.out.println(infineon.findByMaxSalary());
		
		
	}

	

	

}
