package org.campus02.emp;

import java.util.ArrayList;

public class EmployeeManager
{
	private ArrayList<Employee> Entreprise = new ArrayList<Employee>();
	
	public void addEmployee(Employee e)
	{
		Entreprise.add(e);
	}
	
	public Employee findByMaxSalary()
	
	{
		Employee max= Entreprise.get(0);
		
		
		for(Employee wert:Entreprise)
		{
			if(wert.getSalary()>max.getSalary())
			{
				max = wert;
			}
			
			
		}
		return max;
		
		

		
	}
	
	public Employee findByEmpNumber(int number)
	{
		Employee result=null;
		
		for(Employee wert:Entreprise)
		{
			if(wert.getEmpNumber()==number)
			{
				result=wert;
				return result;
			}
			
			
		}
		return result;
	}
	
	public ArrayList<Employee> findByDepartement(String departement)
	{
		ArrayList<Employee> result=new ArrayList<Employee>();
		for(Employee wert:Entreprise)
		{
			if(wert.getDepartement()==departement)
			{
				result.add(wert);
			}
		}
		
		
		
		return result;
	}
	
	
}


