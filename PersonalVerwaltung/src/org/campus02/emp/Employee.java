package org.campus02.emp;

public class Employee
{
	private int empNumber;
	private String name, departement;
	private double salary;
	
	public Employee(int empNumber, String name,String departement, double salary)
	{
		this.empNumber=empNumber;
		this.name=name;
		this.departement=departement;
		this.salary=salary;
	}

	public double getSalary()
	{
		return salary;
	}

	public void setSalary(double salary)
	{
		this.salary = salary;
	}

	public int getEmpNumber()
	{
		return empNumber;
	}

	public String getName()
	{
		return name;
	}

	public String getDepartement()
	{
		return departement;
	}
	public void setDepartement(String departement)
	{
		this.departement = departement;
	}

	@Override
	public String toString()
	{
		return "Employee [empNumber= " + empNumber + ", name=" + name + ", departement=" + departement + ", salary=" + salary + "]";
	}
	
	

}
