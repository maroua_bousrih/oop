package mb.home.account;

public class Account {
	private String id;
	private String name;
	private int balance=0;
	
	public Account (String id, String name)
	{
		this.id=id;
		this.name=name;
	}
	
	public Account (String id, String name, int balance)
	{
		this.id=id;
		this.name=name;
		this.balance=balance;
	}
	
	public String getId()
	{
		return id;
	}
	public String getName()
	{
		return name;
	}
	public int getBalance()
	{
		return balance;
	}
	public int credit (int addamount)
	{
		return balance=balance+addamount;
	}
	public int debit(int subamount)
	{
		if (subamount<= balance)
		{
			balance= balance-subamount;
			System.out.println("After Debit you have now "+ balance);
			
		}
		else
		{
			System.out.println("Amount exceeded balance.you have only "+ balance);
			
		}
		return balance;
		
		
	
	}
	public int transferTo(Account acc, int amount)
	{
		if(amount <= balance)
		{
			acc.balance=acc.balance+amount;
			balance=balance-amount;
			
		}
		else
		{
			System.out.println("Amount exceeded balance");
		}
		return balance;
	}
	public String toString()
	{
		return String.format("Account[id=%s,name=%s, balance=%d]", id,name,balance);
	}

}
