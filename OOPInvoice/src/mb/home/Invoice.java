package mb.home;

public class Invoice {

	private String id;
	private String desc;
	private int qty;
	private double unitPrice;
	
	public Invoice(String id, String desc, int qty, double unitPrice)
	{
		this.id=id;
		this.desc=desc;
		this.qty=qty;
		this.unitPrice=unitPrice;
		
	}
	
	public String getId()
	{
		return id;
	}
	public String getDesc()
	{
		return desc;
	}
	public int getqty()
	{
		return qty;
	}
	
	public void setQty(int newQty)
	{
		qty=newQty;
	}
	public double getUnitPrice()
	{
		return unitPrice;
	}
	public double getTotal()
	{
		return unitPrice*qty;
	}
	public String toString()
	{
		return String.format("Invoice[id=%s,desc=%s,qty=%d,unitPrice=%f]", id,desc,qty,unitPrice);
	}
	
}
