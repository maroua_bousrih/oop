package mb.home.book;

public class Book {
	
	private String name;
	private Author author;
	private double price;
	private int qty;
	
	public Book(String name, Author author, double price)
	{
		this.name=name;
		this.author=author;
		this.price=price;
		qty=0;
	}
	
	public Book(String name, Author author, double price, int qty)
	{
		this.name=name;
		this.author=author;
		this.price=price;
		this.qty=qty;
	}
	
	public String getName()
	{
		return name;
		
	}
	public Author getAuthor()
	{
		return author;
		
	}
	public double getPrice()
	{
		return price;
	}
	public void setPrice(double newPrice)
	{
		price=newPrice;
	}
	
	public int getQty()
	{
		return qty;
	}
	public void setQty(int newQty)
	{
		qty=newQty;
	}
	public String toString()
	{
		return String.format("BOOk[name=%s, Author=%s, price=%f,qty=%d]", name,author,price,qty);
	}
	public String getAuthorName()
	{
		return author.getName();
	}
	public String getAuthorEmail()
	{
		return author.getEmail();
	}
	public Gender getAuthorGender()
	{
		return author.getGender();
		
	}

}
