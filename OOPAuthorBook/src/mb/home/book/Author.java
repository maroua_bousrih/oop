package mb.home.book;

public class Author {
	private String name;
	private String email;
	private Gender gender;
	
	public Author(String name, String email, Gender gender)
	{
		this.name=name;
		this.email=email;
		this.gender=gender;
		
	}
	
	public String getName()
	{
		return name;
	}
	public String getEmail()
	{
		return email;
		
	}
	public void setEmail(String mail)
	{
		email=mail;
	}
	public Gender getGender() 
	{
		return gender;
	}
	public String toString()
	{
		return String.format("Author[name=%s, email=%s, gender=%s]", name, email, gender);
	}
}
