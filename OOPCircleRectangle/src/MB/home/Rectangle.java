package MB.home;

public class Rectangle {
	private float length;
	private float width;

	public Rectangle()
	{
		length=1.0f;
		width=1.0f;
	}
	public Rectangle(float length, float width)
	{
		this.length=length;
		this.width=width;
	}
	public float getLength()
	{
		return length;
	}
	public float getWidth()
	{
		return width;
		
	}
	public void setLength(float newlength)
	{
		length=newlength;
	}
	public void setWidth(float newwidth)
	{
		width=newwidth;
	}
	public float perimeter()
	{
		return 2*length + 2*width;
	}
	public String toString()
	{
		return String.format("Rectangle:[length= %.2f , width= %.2f]", length,width);
	}
}
