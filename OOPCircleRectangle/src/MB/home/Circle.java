package MB.home;

public class Circle {
	
	private String color;
	private double radius;
	
	public Circle()
	{
		this.color="red";
		this.radius=1.0;
	}
	
	public Circle(double radius)
	{
		this.radius=radius;
		this.color="red";
	}
	
	public Circle(double radius, String color)
	{
		this.radius=radius;
		this.color=color;
	}

	public double getRadius()
	{
		return radius;
	}
	public double getArea()
	{
		return Math.PI*radius*radius;
	}
	public String getColor()
	{
		return color;
		
	}
	
	public void setColor(String newcolor)
	{
		color=newcolor;
	}
	public void setRadius(double newradius)
	{
		radius=newradius;
	}
	public String toString()
	{
		return String.format("Circle [radius= %.2f , color= %s]", radius,color);
	}
	public double Circumference()
	{
		return Math.PI*2*radius;
	}
}
