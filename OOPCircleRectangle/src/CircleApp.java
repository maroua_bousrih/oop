import MB.home.Circle;
import MB.home.Rectangle;

public class CircleApp {

	public static void main(String[] args) {

		Circle ff=new Circle();
		System.out.println(ff);
		//System.out.println(ff.getRadius());
		//System.out.println(ff.getArea());
		Circle c =new Circle(4.2);
		System.out.println(c.getArea());
		

		System.out.println("the radius of the circle is " + c.getRadius() + "the area is " + c.getArea() + "the color is "+c.getColor());
		c.setColor("blue");
		//System.out.println("the color has been changed to "+ c.getColor());
		c.setRadius(5.8);
	//	System.out.println("the radius has been changed to "+ c.getRadius());
		System.out.println(c);
	//********************************************************
		
		Rectangle rec=new Rectangle();
		System.out.println(rec);
		Rectangle rec1= new Rectangle(2.5f,3.0f);
		System.out.println(rec1);
		rec1.setLength(3.0f);
		rec1.setWidth(3.5f);
		System.out.println(rec1);
		
	}

}
