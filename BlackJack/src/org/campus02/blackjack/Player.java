package org.campus02.blackjack;

public class Player
{

	private String name;
	private int age;
	
	public Player(String name, int age)
	{
		this.name=name;
		this.age=age;
		
	}

	public String getName()
	{
		return name;
	}

	public int getAge()
	{
		return age;
	}
	
	
	public String toString()
	{
		return String.format("Player[name:%s, age:%d]", this.name,this.age);
	}
}
